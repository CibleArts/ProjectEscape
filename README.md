(https://ciblearts.itch.io/projectescape)

# Engine
Godot 3\

# Description
A classic retro 2d-platform-horror that takes place in a weird world filled with rooms, doors, traps and chests.

# GamePlay
The gamplay allows the playes adapt to any style or strategy, like open fire, stealth, hide and seek, and even parkour. The game is more like solving a puzzle than combat. The other aspects of the game rely on randomness and decission making.

# Character, Goals, Obstacles.
THe player is a raw character with 1 hit point and improves by opening as many chests as possible. The goal is to find the final chest wich is actually a way escape from the tournament. The players objective is filled with traps enemies and other greedy players. The player is limited from constantly moving from one room to another by a timer that gives only a few seconds to stay outside.

# Screenshots
![](ProjectEscape.png)
![](ProjectEscape2.png)
