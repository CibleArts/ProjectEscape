extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	if Global.effects == false:
		$CanvasLayer.hide()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_Single_Player_pressed():
#	SceneChanger.goto_scene("res://scenes/levels/WorldSinglePlayer.tscn", self)
	get_tree().change_scene("res://scenes/levels/WorldSinglePlayer.tscn")

func _on_Options_pressed():
#	SceneChanger.goto_scene("res://scenes/MainMenu/Options.tscn", self)
	get_tree().change_scene("res://scenes/MainMenu/Options.tscn")

func _on_Multiplayer_pressed():
#	SceneChanger.goto_scene("res://scenes/MainMenu/Network_setup.tscn", self)
	get_tree().change_scene("res://scenes/MainMenu/Network_setup.tscn")

func _on_Story_pressed():
#	SceneChangr.goto_scene("res://scenes/MainMenu/Story.tscn", self)
	get_tree().change_scene("res://scenes/MainMenu/Story.tscn")

func _on_Exit_pressed():
	get_tree().quit()
