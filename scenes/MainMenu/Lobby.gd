extends Button

func _ready():
	self.rect_scale.x = 5
	self.rect_scale.y = 5

func _on_Lobby_pressed():
	get_tree().change_scene("res://scenes/MainMenu/Menu.tscn")
#	SceneChanger.goto_scene("res://scenes/MainMenu/Menu.tscn", self)

func _on_CheckButton_toggled(button_pressed):
	if Global.joystick:
		Global.joystick = false
	else:
		Global.joystick = true

func _on_CheckButton2_toggled(button_pressed):
	if Global.effects:
		Global.effects = false
	else:
		Global.effects = true
