extends Node2D

var player_bullet = load("res://Player_bullet.tscn")
var can_shoot = true
var is_reloading = false
const speed = 400

puppet var puppet_velocity = Vector2()

onready var reload_timer = $Reload_timer
onready var shoot_point = $Shoot_point
onready var tween = $Tween

puppet var puppet_rotation = 0

func _process(delta: float) -> void:
	if get_tree().has_network_peer():
		if is_network_master() and visible:
			look_at(get_global_mouse_position())
			if Input.is_action_pressed("fire") and !is_reloading and can_shoot:
				$SoundShot.play()
				rpc("instance_bullet", get_tree().get_network_unique_id())
				is_reloading = true
				reload_timer.start()
		else:
			rotation = lerp_angle(rotation, puppet_rotation, delta * 8)
			"""
			if not tween.is_active():
				move_and_slide(puppet_velocity * speed)
			"""
			
func lerp_angle(from, to, weight):
	return from + short_angle_dist(from, to) * weight

func short_angle_dist(from, to):
	var max_angle = PI * 2
	var difference = fmod(to - from, max_angle)
	return fmod(2 * difference, max_angle) - difference

sync func instance_bullet(id):
	var player_bullet_instance = Global.instance_node_at_location(player_bullet, Persistent_nodes, shoot_point.global_position)
	player_bullet_instance.name = "Bullet" + name + str(Network.networked_object_name_index)
	player_bullet_instance.set_network_master(id)
	player_bullet_instance.player_rotation = rotation
	player_bullet_instance.player_owner = id
	Network.networked_object_name_index += 1

func _on_Reload_timer_timeout():
	is_reloading = false
